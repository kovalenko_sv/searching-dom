'use strict';

const paragraph = [...document.getElementsByTagName('p')];
paragraph.forEach(paragraph => {
    paragraph.style.cssText = 'background-color: #ff0000';
});


const optionList = document.getElementById('optionsList');
console.log(optionsList);

console.log(optionsList.parentNode);
console.log(optionsList.childNodes);

for (let node of optionsList.childNodes) {
    console.log('My node name is:\n'+ node.nodeName + '\nand my type is:\n' + node.nodeType);
}

const text = document.body.querySelector('#testParagraph');
text.innerHTML = 'This is a paragraph';
console.log(text);

const main = document.querySelector('.main-header');
console.log(main.childNodes);
for (let node of main.childNodes) {
    node.className = 'nav-item';
    console.log(node);
}

const options = [...document.querySelectorAll('.options-list-title')];
options.forEach(element => {
    element.classList.remove('options-list-title');
});
